import com.mypackages.Person;

class Hi extends Thread {
	public void run() {
		for (int i = 0; i < 1000; i++) {
			System.out.println("Hi");
		}
	}
}

class Huku extends Thread {
	public void run() {
		for (int i = 0; i < 1000; i++) {
			System.out.println("Huku");
		}
	}
}

class Student extends Person {
	public Student() {
		super("Shailesh", 31, "Nagpur");
	}
}

public class MainClass {

	public static void main(String[] args) {
		
		System.out.println(new Student());
		
//		Hi obj1 = new Hi();
//		Huku obj2 = new Huku();
//		
//		obj1.start();
//		obj2.start();
		
		//obj1.printMsg();
		//obj2.printMsg();
	}
}
