class Bank {
	int balance;
	
	public Bank(int amt) {
		this.balance = amt;
	}
	
	public void deposit(int amt) {
		this.balance += amt; // this.bal = this.bal + amt;
	}
	
	public void withdraw(int amt) {
		this.balance -= amt;
	}
	
	public void showBalance() {
		System.out.println("Current Balance " + this.balance);
	}
	
	@Override
	public String toString() {
		return "Balance: " + this.balance;
	}
}

class SBI extends Bank {
	public SBI(int amt) {
		super(amt);
	}
	
	public void deposit(int amt) {
		amt -= 50; // 50 Rs. deposit charges
		this.balance += amt; // this.bal = this.bal + amt;
	}
	
	public void withdraw(int amt) {
		amt += 100; // 100 Rs. withdraw charges
		this.balance -= amt;
	}
}

public class OverridingDemo {
	public static void main(String[] args) {
		
		SBI sbi_acc = new SBI(1000);
		sbi_acc.deposit(1000);
		sbi_acc.showBalance(); //
		
		sbi_acc.withdraw(500);
		sbi_acc.showBalance();
		
		
		
//		Bank account1 = new Bank(1000);
//		System.out.println(account1);
//		
//		account1.deposit(500);
//		System.out.println(account1);
//		
//		account1.withdraw(300);
//		account1.showBalance();
		
//		Bank account2 = new Bank(2000);
//		System.out.println(account2);
//		
//		int z = add(5,6);
//		int x = 10;
//		Integer y = new Integer(20);
		
		
	}
}
